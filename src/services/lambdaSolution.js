const lambdaSoul = (datas) => {
    let matrix = datas;
    if (datas[0].i) {
        matrix = datas.map(obj => Object.values(obj));
    }
    let data = matrix;
    function fillArray(index) {
        let arr = [];
        for (let i1 = 0; i1 < n; i1++) {
            arr.push(dataValues[i1][index]);
        }
        return arr;
    }
    let dataValues = data;
    let n = dataValues.length; // количество дуг (мягких зависимостей между работами)
    let i = fillArray(0);
    let j = fillArray(1);
    let di = fillArray(2);
    let dj = fillArray(3);
    let aij = fillArray(4);
    let solutionOut = '';
    for (let i1 = 0; i1 < n - 1; i1++) {
        for (let j1 = 0; j1 < n - i1 - 1; j1++) {
            if (j[j1] > j[j1 + 1]) {
                [i[j1], i[j1 + 1]] = [i[j1 + 1], i[j1]];
                [j[j1], j[j1 + 1]] = [j[j1 + 1], j[j1]];
                [di[j1], di[j1 + 1]] = [di[j1 + 1], di[j1]];
                [dj[j1], dj[j1 + 1]] = [dj[j1 + 1], dj[j1]];
                [aij[j1], aij[j1 + 1]] = [aij[j1 + 1], aij[j1]];
            }
        }
    }
    // console.log(i, j);
    let sol = [0];
    let wv = []; // массив содержащий пары [i, di] для каждой рыботы
    let ij = i.concat(j); //все номера работ (получаются из номеров входящих и исходящих дуг)
    let uniqueNumbers = Array.from(new Set(ij));
    let w = uniqueNumbers.length; //количество работ
    for (let i1 = 1; i1 <= w; i1++) { //заполнение массива wv
        for (let i2 = 0; i2 < n; i2++) {
            if (i[i2] === i1) {
                wv.push([i1, di[i2]]);
                break;
            } else if (j[i2] === i1) {
                wv.push([i1, dj[i2]]);
                break;
            }
        }
    }
    // console.log(wv);
    for (let i = 0; i < w; i++) {
        sol.push(wv[i][1]);
    }
    // console.log("lambda =", sol);
    let v = 0;
    let count = 0;
    let arrMin = [0, 0, 0, 0, 0, 0, 0];
    let val = [0, 0, 0, 0, 0, 0, 0, 0];
    let vhod = [0, 0, 0, 0, 0, 0, 0, 0];
    let min = 100000;
    let errorsArc = [];
    let strAnsw = "";
    for (let k = 1; k <= w; k++) {
        count = 0;
        for (let m = 0; m < n; m++) {
            if (j[m] === k) {
                count += 1;
            }
        }
        v = Math.pow(2, count); // показатель количества входящих работ
        if (v === 2) {
            solutionOut += "-------------- Работа " + k + " --------------\n" + "\n";
            // console.log("Работа", k, "--------------");
            for (let s = 0; s < n; s++) {
                if (j[s] === k) {
                    val[0] = s;
                    vhod[0] = i[s];
                    arrMin[0] = aij[s] + sol[k];
                }
            }
            arrMin[1] = sol[vhod[0]] + sol[k];
            // console.log(vhod[0]);
            // console.log(arrMin[0], arrMin[1]);
            strAnsw = "Выгоднее зависимость (" + vhod[0] + ", " + k + ")";
            if (arrMin[0] <= arrMin[1]) {
                sol[k] = arrMin[0];
                strAnsw += " не учитывать";
                errorsArc.push([vhod[0], k]);
            } else {
                sol[k] = arrMin[1];
                strAnsw += " учитывать";
            }
            solutionOut += "Имеем входящую работу " + vhod[0] + "\n" + "\n";
            solutionOut += "Получаем lambda = " + sol[k] + "\n" + "\n";
            solutionOut += strAnsw + "\n" + "\n";
            solutionOut += "\n";
            solutionOut += "\n";
        }
        if (v === 4) {
            solutionOut += "-------------- Работа " + k + " --------------\n" + "\n";
            for (let s = 0; s < n; s++) {
                if (j[s] === k) {
                    vhod[1] = i[s];
                    val[1] = s;
                }
            }
            for (let s = 0; s < n; s++) {
                if (j[s] === k) {
                    if (i[s] !== vhod[1]) {
                        vhod[0] = i[s];
                        val[0] = s;
                    }
                }
            }
            arrMin[0] = sol[k] + aij[val[0]] + aij[val[1]];
            arrMin[1] = sol[vhod[0]] + aij[val[1]] + sol[k];
            arrMin[2] = sol[vhod[1]] + aij[val[0]] + sol[k];
            arrMin[3] = sol[vhod[0]] + sol[k] > sol[vhod[1]] + sol[k] ? sol[vhod[0]] + sol[k] : sol[vhod[1]] + sol[k];
            let strAnsw = "";
            let strAnsw1 = "Выгоднее учитывать только зависимость (";
            let strAnsw2 = "Выгоднее обе зависимости (" + vhod[0] + ", " + k + ") и (" + vhod[1] + ", " + k + ")";
            strAnsw1 += vhod[0] + ", " + k + ")";
            for (let d = 0; d < 4; d++) {
                if (arrMin[d] < min) {
                    min = arrMin[d];
                }
            }
            if (arrMin[0] === min) {
                strAnsw = strAnsw2 + " не учитывать";
                errorsArc.push([vhod[0], k]);
                errorsArc.push([vhod[1], k]);
            } else if (arrMin[1] === min) {
                strAnsw = strAnsw1 + vhod[0] + ", " + k + ")";
                errorsArc.push([vhod[1], k]);
            } else if (arrMin[2] === min) {
                strAnsw = strAnsw1 + vhod[1] + ", " + k + ")";
                errorsArc.push([vhod[0], k]);
            } else if (arrMin[3] === min) {
                strAnsw = strAnsw2 + " учитывать";
            }
            sol[k] = min;
            min = 100000;
            // console.log(strAnsw);
            solutionOut += "Имеем входящие работы " + vhod[0] + " и " + vhod[1] + "\n" + "\n";
            solutionOut += "Получаем lambda = " + sol[k] + "\n" + "\n";
            solutionOut += strAnsw + "\n" + "\n";
            solutionOut += "\n";
            solutionOut += "\n";
        }
        if (v === 8) {
            // console.log();
            solutionOut += "-------------- Работа " + k + " --------------\n" + "\n";
            // console.log("Работа", k, "--------------");

            for (let s = 0; s < n; s++) {
                if (j[s] === k) {
                    vhod[2] = i[s];
                    val[2] = s;
                }
            }

            for (let s = 0; s < n; s++) {
                if (j[s] === k) {
                    if (i[s] !== vhod[2]) {
                        vhod[1] = i[s];
                        val[1] = s;
                    }
                }
            }

            for (let s = 0; s < n; s++) {
                if (j[s] === k) {
                    if (i[s] !== vhod[2] && i[s] !== vhod[1]) {
                        vhod[0] = i[s];
                        val[0] = s;
                    }
                }
            }

            let strAnsw1 = "Выгоднее учитывать только зависимость (";
            let strAnsw2 = "Выгоднее учитывать только зависимости (";
            let strAnsw3 = "Выгоднее все зависимости (" + vhod[0] + ", " + k + ") и " + "(" + vhod[2] + ", " + k + ") и" + "(" + vhod[1] + ", " + k + ")";
            //aij[val[0]] - 16, vhod[1] - 56, vhod[2] - 36
            // Все 3 зависимости не учитываем
            arrMin[0] = sol[k] + aij[val[0]] + aij[val[2]] + aij[val[1]];
            // Учитываем зависимость vhod[0]
            arrMin[1] = sol[k] + aij[val[2]] + aij[val[1]] + sol[vhod[0]];
            // Учитываем зависимость vhod[2]
            arrMin[2] = sol[k] + aij[val[0]] + aij[val[1]] + sol[vhod[2]];
            // Учитываем зависимость vhod[1]
            arrMin[3] = sol[k] + aij[val[0]] + aij[val[2]] + sol[vhod[1]]; // sol[vhod[1]] = l5
            // Учитываем зависимости vhod[0] и vhod[2] 16 и 36
            arrMin[4] = (sol[k] + aij[val[1]] + sol[vhod[0]]) >= sol[vhod[2]] ? sol[k] + aij[val[1]] + sol[vhod[2]] : sol[k] + aij[val[1]] + sol[vhod[0]];

            // Учитываем зависимости vhod[0] и vhod[1] 16 и 56
            arrMin[5] = (sol[k] + aij[val[2]] + sol[vhod[0]]) >= sol[vhod[1]] ? sol[k] + aij[val[2]] + sol[vhod[1]] : sol[k] + aij[val[2]] + sol[vhod[0]];
            // console.log(sol[k] + aij[val[2]] + sol[vhod[0]], sol[vhod[1]]);
            // console.log(sol[k], "+", aij[val[2]], "+", sol[vhod[1]], "или", sol[vhod[0]]);
            // Учитываем зависимости vhod[2] и vhod[1]  36 и 56
            arrMin[6] = (sol[k] + aij[val[0]] + sol[vhod[2]]) >= sol[vhod[1]] ? sol[k] + aij[val[0]] + sol[vhod[1]] : sol[k] + aij[val[0]] + sol[vhod[2]];
            // Учитываем все 3 зависимости
            if (sol[vhod[0]] > sol[vhod[1]] && sol[vhod[2]] > sol[vhod[0]]) {
                arrMin[7] = sol[k] + sol[vhod[0]];
            } else if (sol[vhod[1]] > sol[vhod[0]] && sol[vhod[1]] > sol[vhod[2]]) {
                arrMin[7] = sol[k] + sol[vhod[1]];
            } else {
                arrMin[7] = sol[k] + sol[vhod[2]];
            }
            for (let d = 0; d < 8; d++) {
                if (arrMin[d] < min) {
                    min = arrMin[d];
                }
            }
            sol[k] = min;
            if (arrMin[0] === min) {
                strAnsw = strAnsw3 + " не учитывать";
                errorsArc.push([vhod[0], k]);
                errorsArc.push([vhod[2], k]);
                errorsArc.push([vhod[1], k]);
            } else if (arrMin[1] === min) {
                strAnsw = strAnsw1 + vhod[0] + ", " + k + ")";
                errorsArc.push([vhod[2], k]);
                errorsArc.push([vhod[1], k]);
            } else if (arrMin[2] === min) {
                strAnsw = strAnsw1 + vhod[2] + ", " + k + ")";
                errorsArc.push([vhod[0], k]);
                errorsArc.push([vhod[1], k]);
            } else if (arrMin[3] === min) {
                strAnsw = strAnsw1 + vhod[1] + ", " + k + ")";
                errorsArc.push([vhod[0], k]);
                errorsArc.push([vhod[2], k]);
            } else if (arrMin[4] === min) {
                strAnsw = strAnsw2 + vhod[0] + ", " + k + ") и " + "(" + vhod[2] + ", " + k + ")";
                errorsArc.push([vhod[1], k]);
            } else if (arrMin[5] === min) {
                strAnsw = strAnsw2 + vhod[0] + ", " + k + ") и " + "(" + vhod[1] + ", " + k + ")";
                errorsArc.push([vhod[2], k]);
            } else if (arrMin[6] === min) {
                strAnsw = strAnsw2 + vhod[2] + ", " + k + ") и " + "(" + vhod[1] + ", " + k + ")";
            } else if (arrMin[7] === min) {
                strAnsw = strAnsw3 + " учитывать";
            }
            min = 100000;
            solutionOut += "Имеем входящие работы " + vhod[0] + " , " + vhod[1] + " и " + vhod[2] + "\n" + "\n";
            solutionOut += "Получаем lambda = " + sol[k] + "\n" + "\n";
            solutionOut += strAnsw + "\n" + "\n";
            solutionOut += "\n";
            solutionOut += "\n";
        }
    }
    sol.shift();
    let arr1 = [];
    for (let d = 0; d < 6; d++) {
        arr1.push(wv[d][0]);
    }
    dataValues = dataValues.map(item => item.slice());
    let remArcNumb = [];
    let answerDataValues = [];
    let tmpArr = [];
    for (let k1 = 0; k1 < errorsArc.length; k1++) {
        for (let i1 = 0; i1 < n; i1++) {
            if (dataValues[i1][0] === errorsArc[k1][0] && dataValues[i1][1] === errorsArc[k1][1]) {
                tmpArr.push([dataValues[i1][1], dataValues[i1][4]]);
            }
        }
    }
    for (let k1 = 0; k1 < tmpArr.length; k1++) {
        for (let i1 = 0; i1 < n; i1++) {
            if (dataValues[i1][0] === tmpArr[k1][0]) {
                dataValues[i1][2] += tmpArr[k1][1];
            }
        }
    }
    for (let k1 = 0; k1 < tmpArr.length; k1++) {
        for (let i1 = 0; i1 < n; i1++) {
            if (dataValues[i1][1] === tmpArr[k1][0]) {
                dataValues[i1][3] += tmpArr[k1][1];
            }
        }
    }
    for (let k1 = 0; k1 < errorsArc.length; k1++) {
        for (let i1 = 0; i1 < n; i1++) {
            if (dataValues[i1][0] === errorsArc[k1][0] && dataValues[i1][1] === errorsArc[k1][1]) {
                remArcNumb.push(i1);
            }
        }
    }
    let specificRows = [];
    for (let k1 = 0; k1 < n; k1++) {
        if (remArcNumb.includes(k1)) {
            // nothing = []; // Не нужно присваивать пустой массив, если k1 есть в remArcNumb
        } else {
            answerDataValues.push(dataValues[k1]);
            specificRows.push(k1);
        }
    }
    console.log("Минимальная продолжительность проекта = ", Math.max(...sol));
    let obj = {
        inputMatrix: matrix,
        outputMatrix: dataValues,
        specificRows: specificRows,
        lambda: sol,
        solutionOut: solutionOut,
        maxDuration: Math.max(...sol),
        worksInfo: wv
    }
    return obj;
}
const CalculateService = {
    lambdaSoul,
}
export default CalculateService;