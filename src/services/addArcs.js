function findIsolatedVertices(graph, vertices) {
    let outgoingVertices = new Set();
    graph.forEach(edge => {
        outgoingVertices.add(edge[0]);
    });
    let isolatedVertices = vertices.filter(vertex => !outgoingVertices.has(vertex));
    return isolatedVertices;
}
function findReachableVertices(graph, vertices) {
    let reachableVertices = {};
    vertices.forEach(vertex => {
        reachableVertices[vertex] = [];
    });
    graph.forEach(edge => {
        reachableVertices[edge[1]].push(edge[0]);
    });
    function updateReachableVertices(vertex) {
        reachableVertices[vertex].forEach(parentVertex => {
            reachableVertices[vertex] = [...new Set(reachableVertices[vertex].concat(reachableVertices[parentVertex]))];
        });
    }
    vertices.forEach(vertex => {
        updateReachableVertices(vertex);
    });
    return reachableVertices;
}
function findAllPaths(graph, start, end, currentPath = [], allPaths = []) {
    currentPath.push(start);
    if (start === end) {
        allPaths.push([...currentPath]);
    } else {
        for (let edge of graph) {
            if (edge[0] === start) {
                findAllPaths(graph, edge[1], end, currentPath, allPaths);
            }
        }
    }
    currentPath.pop();
    return allPaths;
}
function connectWorks(graph1, T) {
    const graph = [...graph1]
    let connectedGraph = [];
    let worksInfo = []
    graph.forEach(graphItem => {
        if (!worksInfo.some(item => item[0] === graphItem[0])) {
            worksInfo.push([graphItem[0], graphItem[2]])
        }
        if (!worksInfo.some(item => item[0] === graphItem[1])) {
            worksInfo.push([graphItem[1], graphItem[3]])
        }
    });
    //массив номеров работ [1, 2, 3,...]
    let works = []
    worksInfo.forEach(item => works.push(item[0]))
    works.sort((a, b) => a - b)
    const lastNumberWork = Math.max(...works) + 1
    //Для начала находим вершины из которых ничего не выходит, чтобы связать их с конечной (выходной) вершиной
    const isolatedVertices = findIsolatedVertices(graph, works);
    console.log("Добавили дуги до выходящей точки:");
    let addedEdges = []
    isolatedVertices.forEach(work => {
        // if (work > 1) {
        let edge = [work, lastNumberWork, worksInfo.filter(item => item[0] === work)[0][1], 0, 0]
        console.log(edge);
        graph.push(edge)
        connectedGraph.push(edge);
        addedEdges.push(edge)
        // }
    })
    works.push(lastNumberWork)
    worksInfo.push([lastNumberWork, 0])
    //создание объекта достижимости каждой вершины
    let reachableVertices = findReachableVertices(graph, works);
    let n = graph.length
    for (let i of works) {
        let di = worksInfo.filter(item => item[0] === i)[0][1]
        // console.log("Проверяем работу", i);
        for (let j of works) {
            //проверка, что вершина j достижима из вершины i
            if (reachableVertices[j].includes(i)) {
                let dj = worksInfo.filter(item => item[0] === j)[0][1]
                if (!graph.some(item => {
                    return (item[0] === i && item[1] === j)
                })) {
                    // console.log("Идем в работу", j);
                    let startVertex = i;
                    let endVertex = j;
                    let allPaths = findAllPaths(graph, startVertex, endVertex)
                    let arrSum = [];
                    // Для каждого пути проверяем его продолжительность чтобы выбрать минимальную продолжительность
                    allPaths.forEach(path => {
                        let sum = 0;
                        for (let k = 0; k < path.length - 1; k++) {
                            let workDur = worksInfo.filter(item => item[0] === path[k])[0][1]
                            sum += workDur
                        }
                        arrSum.push(sum)
                    })
                    let min = 10000
                    let indexMin = 0
                    for (let i = 0; i < arrSum.length; i++) {
                        if (arrSum[i] < min) {
                            min = arrSum[i]
                            indexMin = i
                        }
                    }
                    // номер предпоследней работы, для того чтобы взять продолжительность дуги
                    const numbPrevWork = allPaths[indexMin][allPaths[indexMin].length - 2]
                    let duration = graph.filter(item => item[0] === numbPrevWork && item[1] === j)[0][4]
                    console.log("из работы", i, "в работу", j, "ведут пути:", allPaths);
                    console.log("Продолжительность путей: ", ...arrSum);
                    console.log("Продолжительность минимального и путь: ", min);
                    if (min < T) {
                        //проверка что не добавляем удаленную дугу
                        // if (!graph.some(item => item[0] === i && item[1] === j)) {
                        let edge = [i, j, di, dj, duration]
                        connectedGraph.push(edge)
                        console.log("Добавим дугу", edge);
                        console.log("С продолжительностью", duration);
                        console.log();
                        if (edge[3] === 0 && edge[4] === 0) {
                            addedEdges.push(edge)
                        }
                    }
                } else {
                    // console.log("дуга", i, j, "существует!!!!");
                }
            }
        }
    }
    const ibj = {
        connectedGraph: connectedGraph,
        worksInfo: worksInfo,
        addedEdges: addedEdges
    }
    return ibj;
}
const ConnectWorks = {
    connectWorks
}
export default ConnectWorks;

