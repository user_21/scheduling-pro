function shortestPath(graph1, start, end) {
    let graph = []
    graph1.forEach(item => {
        graph.push([item[0], item[1], item[4]])
    })
    const vertices = new Set();
    const distances = {};
    const previous = {};
    // Инициализация начальных расстояний
    graph.forEach(([i, j, weight]) => {
        vertices.add(i);
        vertices.add(j);
        distances[i] = Infinity;
        distances[j] = Infinity;
    });
    distances[start] = 0;
    while(vertices.size > 0) {
        let minVertex = null;
        vertices.forEach(vertex => {
            if (!minVertex || distances[vertex] < distances[minVertex]) {
                minVertex = vertex;
            }
        });
        if (minVertex === end || distances[minVertex] === Infinity) {
            break;
        }
        vertices.delete(minVertex);
        graph.filter(([i, j]) => i === minVertex).forEach(([i, j, weight]) => {
            const alt = distances[minVertex] + weight;
            if (alt < distances[j]) {
                distances[j] = alt;
                previous[j] = minVertex;
            }
        });
    }
    const path = [];
    let current = end;
    while (current) {
        path.unshift(current);
        current = previous[current];
    }
    const pathLength = distances[end];
    return { path, length: pathLength };
}
const SortestPath = {
    shortestPath
}
export default SortestPath;


