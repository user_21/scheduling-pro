import CalculateService from "./lambdaSolution";
import ConnectWorks from "./addArcs";
import SortestPath from "./shortestPath";
const optimalIncision = (data, T) => {
    let graph = [...data]
    if (data[0].i) {
        graph = data.map(obj => Object.values(obj));
    }
    function hasCycle(graph) {
        const visited = new Set();
        const recursionStack = new Set();
        function hasCycleHelper(node) {
            if (recursionStack.has(node)) {
                return true;
            }
            if (visited.has(node)) {
                return false;
            }
            visited.add(node);
            recursionStack.add(node);
            for (const [start, end] of graph) {
                if (start === node) {
                    if (hasCycleHelper(end)) {
                        return true;
                    }
                }
            }
            recursionStack.delete(node);
            return false;
        }
        for (const [start, end] of graph) {
            if (!visited.has(start)) {
                if (hasCycleHelper(start)) {
                    return true;
                }
            }
        }
        return false;
    }
    let result = []
    const check1 = () => {
        graph.forEach((item, index) => {
            let arr = [...graph]
            arr.splice(index, 1)
            const hasCycles = hasCycle(arr);
            if (!hasCycles) {
                // console.log('Граф не содержит контуров - ', [item[0], item[1]]);
                result.push({
                    val: item[4],
                    arcs: [[item[0], item[1]]],
                })
            }
        })
    }
    const check2 = () => {
        graph.forEach((item, index) => {
            graph.forEach((item1, index1) => {
                if (index != index1 && index1 > index) {
                    let arr = [...graph]
                    let a = arr.splice(index, 1)[0]
                    let b = arr.splice(index1 - 1, 1)[0]
                    const hasCycles = hasCycle(arr);
                    if (!hasCycles) {
                        // console.log('Граф не содержит контуров - ', [item[0], item[1]], [item1[0], item1[1]], item[2] + item1[2]);
                        result.push({
                            val: item[4] + item1[4],
                            arcs: [[item[0], item[1]], [item1[0], item1[1]]],
                        })
                    }
                }
            })
        })
    }
    const check3 = () => {
        graph.forEach((item, index) => {
            graph.forEach((item1, index1) => {
                graph.forEach((item2, index2) => {
                    if (index2 > index1 && index1 > index) {
                        let arr = [...graph]
                        let a = arr.splice(index, 1)[0]
                        let с = arr.splice(index2 - 2, 1)[0]
                        let b = arr.splice(index1 - 1, 1)[0]
                        const hasCycles = hasCycle(arr);
                        if (!hasCycles) {
                            // console.log('Граф не содержит контуров - ', [item[0], item[1]], [item1[0], item1[1]], [item2[0], item2[1]], item[2] + item1[2] + item2[2]);
                            result.push({
                                val: item[4] + item1[4] + item2[4],
                                arcs: [[item[0], item[1]], [item1[0], item1[1]], [item2[0], item2[1]]],
                            })
                        }
                    }
                })
            })
        })
    }
    const check4 = () => {
        graph.forEach((item, index) => {
            graph.forEach((item1, index1) => {
                graph.forEach((item2, index2) => {
                    graph.forEach((item3, index3) => {
                        if (index3 > index2 && index2 > index1 && index1 > index) {
                            let arr = [...graph]
                            let a = arr.splice(index, 1)[0]
                            let b = arr.splice(index1 - 1, 1)[0]
                            let с = arr.splice(index2 - 2, 1)[0]
                            let в = arr.splice(index3 - 3, 1)[0]
                            const hasCycles = hasCycle(arr);
                            if (!hasCycles) {
                                // console.log('Граф не содержит контуров - ', [item[0], item[1]], [item1[0], item1[1]], [item2[0], item2[1]], [item3[0], item3[1]], item[2] + item1[2] + item2[2] + item3[2]);
                                result.push({
                                    val: item[4] + item1[4] + item2[4] + item3[4],
                                    arcs: [[item[0], item[1]], [item1[0], item1[1]], [item2[0], item2[1]], [item3[0], item3[1]]],
                                })
                            }
                        }
                    })
                })
            })
        })
    }
    let hasCycleGraph = hasCycle(graph)
    // let outputData = [...graph];
    let outputData1 = [...graph];
    let resultOptimal;
    if (hasCycleGraph) {
        check1(data)
        check2(data)
        check3(data)
        check4(data)
        result.sort((a, b) => a.val - b.val)
        let value1 = result[0].val;
        let value2;
        for (let i = 0; i < result.length; i++) {
            if (value1 != result[i].val) {
                value2 = result[i].val;
                break;
            }
        }
        resultOptimal = result.filter(item => [value1, value2].includes(item.val))
        outputData1 = graph.filter(item => !resultOptimal[0].arcs.some(subarray => subarray.every((value, index) => value === item[index])))
        console.log("Граф содержит контуры");
        console.log("Удален оптимальный разрез:", resultOptimal[0].arcs, "пропускной способностью", resultOptimal[0].val);
    } else {
        console.log("Граф не содержит контуры");
    }
    let сonnectWorks = null
    let path = {}
    const lambdaSolution = CalculateService.lambdaSoul(outputData1)
    let needOptimization = false
    let specificRows = []
    if (lambdaSolution.maxDuration > T) {
        console.log("Требуется оптимизация ");
        needOptimization = true
        сonnectWorks = ConnectWorks.connectWorks(outputData1, T);
        outputData1.push(...(сonnectWorks.connectedGraph))
        let maxFirstElement = null;
        // Проходимся по каждому подмассиву и находим максимальное значение среди первых элементов
        outputData1.forEach(subArray => {
            let firstElement = subArray[1]; // Получаем первый элемент подмассива
            // Проверяем, является ли текущее значение максимальным
            if (maxFirstElement === null || firstElement > maxFirstElement) {
                maxFirstElement = firstElement; // Обновляем максимальное значение
            }
        });
        console.log(maxFirstElement);
        path = SortestPath.shortestPath(outputData1, 1, maxFirstElement)
        // console.log(outputData1);
        // console.log(сonnectWorks.addedEdges);
        сonnectWorks.addedEdges.forEach(subarrayToRemove => {
            let index = outputData1.findIndex(subarray => JSON.stringify(subarray) === JSON.stringify(subarrayToRemove));
            if (index !== -1) {
                outputData1.splice(index, 1);
            }
        });
        console.log(outputData1);
        //необходимо проверить сколько работ из которых можно начать строить путь и предложить разные варианты кратчайшего пути
        console.log("Кратчайший путь:", path.path);
        console.log("Длина пути: (увеличение затрат при продолжительности проекта)", T, "дней", "=", path.length);
    } else {
        console.log("Продолжительность проекта меньше заданной, оптимизация не требуется");
    }
    if (needOptimization) {
        // path.path
        for (let i = 0; i < path.path.length - 1; i++) {
            outputData1.forEach((item, index) => {
                if (item[0] === path.path[i] && item[1] === path.path[i + 1]) {
                    specificRows.push(index)
                }
            })
        }
        console.log(specificRows);
    }
    let arcs = []
    outputData1.forEach(item => {
        arcs.push([item[0], item[1], item[4]])
    })
    let obj = {
        inputMatrix: graph,
        outputMatrix: outputData1,
        path: path,
        worksInfo: needOptimization ? сonnectWorks.worksInfo : null,
        arcs: arcs,
        needOptimization: needOptimization,
        specificRows: specificRows,
        lambdaSolutionDuration: lambdaSolution.maxDuration
    }
    return obj;
}
const OptimalIncision = {
    optimalIncision
}
export default OptimalIncision;