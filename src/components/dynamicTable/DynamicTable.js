import "./dynamicTable.scss";
import React, { useState } from 'react';
import { useContext } from "react";
import { Context } from "../../context";
import { useEffect } from "react";
const DynamicTable = ({ mod }) => {
    let { modalActive, data, toggleData, allData, toggleAllData, toggleModal } = useContext(Context);
    const [tableData, setTableData] = useState(data.allData)
    const [tableName, setTableName] = useState(data.name)
    useEffect(() => {
        if (mod == "new") {
            setTableData([{ id: 1, i: "", j: "", di: "", dj: "", aij: "" }])
            setTableName("Таблица 1")
        } else {
            setTableData(data.allData)
            setTableName(data.name)
        }
        const handleKeyDown = (event) => {
            if (event.key === "Escape") {
                const currentUrl = window.location.href;
                if (currentUrl.includes("data") && modalActive) {
                    clickResetButton();
                }
            }
        };
        document.addEventListener('keydown', handleKeyDown);

        return () => {
            document.removeEventListener('keydown', handleKeyDown);
        };
    }, [data])
    const handleInputChange = (event, id, fieldName) => {
        const input = event.target.value;
        const filteredInput = Number(input.replace(/\D/g, ''));
        const updatedTableData = tableData.map(row =>
            row.id === id ? { ...row, [fieldName]: filteredInput } : row
        );

        setTableData(updatedTableData);
    };
    const clickAddButton = () => {
        setTableData([...tableData, { id: tableData.length + 1, i: "", j: "", di: "", dj: "", aij: "" }])
    }
    const clickDeleteButton = (id) => {
        const updatedTableData = tableData.filter(row => row.id !== id);
        setTableData(updatedTableData);
    }
    const clickSaveButton = () => {
        const uniqueId = Date.now().toString(36) + Math.random().toString(36).substr(2);

        const uploadDate = new Date();
        const formattedDate = new Intl.DateTimeFormat('ru-RU', {
            day: 'numeric',
            month: 'numeric',
            year: 'numeric',
            hour: 'numeric',
            minute: 'numeric'
        }).format(uploadDate);

        if (mod == "new") {
            toggleAllData([...allData, { id: uniqueId, name: tableName, date: formattedDate, status: false, type: selectedOption, allData: tableData }])
        } else {
            let obj = Object.assign({}, data);;
            obj.allData = tableData;
            obj.name = tableName;
            obj.date = formattedDate;

            if (data.status) {
                obj.id = uniqueId;
                obj.status = false;
                toggleAllData([...allData, obj])
            } else {
                toggleData(obj);
            }
        }
        toggleModal();
    }
    const clickResetButton = () => {
        toggleModal();
        if (mod !== "new") {
            setTableData(data.allData);
            setTableName(data.name)
        } else {
            setTableData([{ id: data.length + 1, i: "", j: "", di: "", dj: "", aij: "" }])
            setSelectedOption("a")
        }
    }
    const [selectedOption, setSelectedOption] = useState("a");
    const handleOptionChange = (e) => {
        setSelectedOption(e.target.value);
    };
    return (
        <div className={`dynamic-table-wrapper ${mod}`}>
            {data.status &&
                <div className="dynamic-table__label">
                    <div className="dynamic-table__label-icon"></div>
                    Сохранение изменений в таблице со статусом «Использовано» приведет к созданию новой таблицы.
                </div>
            }
            <div className="dynamic-table__buttons-panel">
                <input
                    className="dynamic-table__name-input"
                    value={tableName}
                    onChange={e => setTableName(e.target.value)}
                    type="text"
                    placeholder="Название"
                />
                <div className="dynamic-table__inputs">
                    <label>
                        <input
                            type="radio"
                            value="a"
                            checked={selectedOption === "a"}
                            onChange={handleOptionChange}
                        />
                        {/* a<sub>ij</sub> */}
                        Продолжительность
                    </label>
                    <label>
                        <input
                            type="radio"
                            value="b"
                            checked={selectedOption === "b"}
                            onChange={handleOptionChange} Ё
                        />
                        {/* b<sub>ij</sub> */}
                        Затраты
                    </label>
                </div>
                <button
                    className="dynamic-table__add-button"
                    onClick={() => clickAddButton()}
                >
                    <div className="add-button__icon"></div>
                    <div>Добавить строку</div>
                </button>
            </div>
            <div className="dynamic-table">
                <div className="dynamic-table__row dynamic-table__head-row">
                    <div className="dynamic-table__cell">
                        <div className="dynamic-table__cell-text">№</div>
                    </div>
                    <div className="dynamic-table__cell">
                        <div className="dynamic-table__cell-text">i</div>
                    </div>
                    <div className="dynamic-table__cell">
                        <div className="dynamic-table__cell-text">j</div>
                    </div>
                    <div className="dynamic-table__cell">
                        <div className="dynamic-table__cell-text">d<sub>i</sub></div>
                    </div>
                    <div className="dynamic-table__cell">
                        <div className="dynamic-table__cell-text">d<sub>j</sub></div>
                    </div>
                    <div className="dynamic-table__cell">
                        <div className="dynamic-table__cell-text">
                            {mod === "new" &&
                                <div>
                                    {selectedOption}<sub>ij</sub>
                                </div>
                            }
                            {mod !== "new" &&
                                <div>
                                    {data.type}<sub>ij</sub>
                                </div>
                            }
                        </div>
                    </div>
                    <div className="dynamic-table__cell">
                        <div className="dynamic-table__cell-text">Удалить</div>
                    </div>
                </div>
                {tableData?.map((row, index) => {
                    return (
                        <div className="dynamic-table__row">
                            <div className="dynamic-table__cell dynamic-table__cell-numb">
                                <input
                                    className="dynamic-table__input"
                                    value={index + 1}
                                    type="text"
                                />
                            </div>
                            <div className="dynamic-table__cell">
                                <input
                                    className="dynamic-table__input"
                                    value={row.i}
                                    onChange={e => handleInputChange(e, row.id, 'i')}
                                    type="text"
                                />
                            </div>
                            <div className="dynamic-table__cell">
                                <input
                                    className="dynamic-table__input"
                                    value={row.j}
                                    onChange={e => handleInputChange(e, row.id, 'j')}
                                    type="text"
                                />
                            </div>
                            <div className="dynamic-table__cell">
                                <input
                                    className="dynamic-table__input"
                                    value={row.di}
                                    onChange={e => handleInputChange(e, row.id, 'di')}
                                    type="text"
                                />
                            </div>
                            <div className="dynamic-table__cell">
                                <input
                                    className="dynamic-table__input"
                                    value={row.dj}
                                    onChange={e => handleInputChange(e, row.id, 'dj')}
                                    type="text"
                                />
                            </div>
                            <div className="dynamic-table__cell">
                                <input
                                    className="dynamic-table__input"
                                    value={data.type == "a" ? row.aij : row.bij}
                                    onChange={e => handleInputChange(e, row.id, data.type == "a" ? "aij" : "bij")}
                                    type="text"
                                />
                            </div>
                            <div
                                className="dynamic-table__delete-btn"
                                onClick={() => clickDeleteButton(row.id)}
                            >
                                <div className="dynamic-table__delete-btn-img"></div>
                            </div>
                        </div>
                    );
                })}
            </div>
            <div className="dynamic-table__bottom-buttons-panel">
                <button
                    className="dynamic-table__reset-button dynamic-table__add-button"
                    onClick={() => clickResetButton()}
                >
                    <div className="reset-button__icon add-button__icon"></div>
                    <div>Отмена</div>
                </button>
                <button
                    className="dynamic-table__save-button dynamic-table__add-button"
                    onClick={() => clickSaveButton()}
                >
                    <div className="save-button__icon add-button__icon"></div>
                    <div>Сохранить</div>
                </button>
            </div>
        </div>
    );
};
export default DynamicTable;