import "./taskInfo.scss";
import { useContext, useState } from "react";
import { Context } from "../../context";
import { useParams } from 'react-router-dom';
import { useEffect } from "react";
import CalculateService from "../../services/lambdaSolution";
import OptimalIncision from "../../services/optimalIncision";
import Graph from "../graph/MyGraph";
let graph1 = {nodes: [],edges: []}
let graph2 = {nodes: [],edges: []}
const TaskInfo = () => {
    const { number } = useParams();
    const [taskData, setTaskData] = useState();
    const [answerData, setAnswerData] = useState();
    const [modalActive, setModalActive] = useState(false);
    useEffect(() => {
        let taskData1 = tasksData.find(item => item.number == number)
        if (taskData1) {
            taskData1.status = true;
        } else {
            return
        }
        let answer;
        if (taskData1.type === "a") {
            answer = CalculateService.lambdaSoul(taskData1.data);
            const lamdaArray = [...answer.lambda];
            taskData1.dur = lamdaArray.sort((a, b) => a - b).pop();
            let arcs = answer.inputMatrix
            let worksInfo = answer.worksInfo
            let specificRows = answer.specificRows
            graph1 = {nodes: [],edges: []}
            arcs.forEach((item, index) => {
                const uniqueId1 = Date.now().toString(36) + Math.random().toString(36).substr(2);
                graph1.edges.push({
                    from: item[0],
                    to: item[1],
                    label: item[4].toString(),
                    id: uniqueId1,
                })

            })
            worksInfo.forEach(item => {
                graph1.nodes.push({
                    id: item[0],
                    label: `  ${item[0]}  \n  ${item[1]}  `,
                    group: 1,
                    border: 'black'
                })
            })
            let arcs1 = []
            answer.outputMatrix.forEach((item, index) => {
                if (specificRows.includes(index)) {
                    arcs1.push(item)
                }
            })
            graph2 = {nodes: [],edges: []}
            arcs1.forEach((item, index) => {
                const uniqueId1 = Date.now().toString(36) + Math.random().toString(36).substr(2);
                graph2.edges.push({
                    from: item[0],
                    to: item[1],
                    label: item[4].toString(),
                    id: uniqueId1,
                    width: 2
                })
            })
            let arrWorks = []
            arcs1.forEach(graphItem => {
                if (!arrWorks.some(item => item[0] === graphItem[0])) {
                    arrWorks.push([graphItem[0], graphItem[2]])
                }
                if (!arrWorks.some(item => item[0] === graphItem[1])) {
                    arrWorks.push([graphItem[1], graphItem[3]])
                }
            })
            arrWorks.forEach(item => {
                graph2.nodes.push({
                    id: item[0],
                    label: `  ${item[0]}  \n  ${item[1]}  `,
                    group: 1,
                    border: 'black'
                })
            })
        } else {
            answer = OptimalIncision.optimalIncision(taskData1.data, taskData1.limit);
            if (answer.needOptimization) {
                taskData1.expenses = answer.path.length
            } else {
                taskData1.expenses = 0
                taskData1.dur = answer.lambdaSolutionDuration
            }
            let arcs = []
            let worksInfo = []
            let specificRows = answer.specificRows
            graph2 = {nodes: [],edges: []}
            answer.outputMatrix.forEach((item, index) => {
                if (specificRows.includes(index)) {
                    arcs.push(item)
                }
            })
            arcs.forEach(graphItem => {
                if (!worksInfo.some(item => item[0] === graphItem[0])) {
                    worksInfo.push([graphItem[0], graphItem[2]])
                }

                if (!worksInfo.some(item => item[0] === graphItem[1])) {
                    worksInfo.push([graphItem[1], graphItem[3]])
                }
            })
            if (answer.needOptimization) {
                arcs.forEach(item => {
                    const uniqueId1 = Date.now().toString(36) + Math.random().toString(36).substr(2);
                    graph2.edges.push({
                        from: item[0],
                        to: item[1],
                        label: item[4].toString(),
                        id: uniqueId1,
                        width: 2
                    })
                })
                worksInfo.forEach(item => {
                    graph2.nodes.push({
                        id: item[0],
                        label: `  ${item[0]}  \n  ${item[1]}  `,
                        group: 1,
                    })
                })
            } else {
                answer.outputMatrix.forEach(item => {
                    const uniqueId1 = Date.now().toString(36) + Math.random().toString(36).substr(2);
                    graph2.edges.push({
                        from: item[0],
                        to: item[1],
                        label: item[4].toString(),
                        id: uniqueId1,
                        width: 1
                    })
                })
                worksInfo = []
                answer.outputMatrix.forEach(graphItem => {
                    if (!worksInfo.some(item => item[0] === graphItem[0])) {
                        worksInfo.push([graphItem[0], graphItem[2]])
                    }
                    if (!worksInfo.some(item => item[0] === graphItem[1])) {
                        worksInfo.push([graphItem[1], graphItem[3]])
                    }
                })
                worksInfo.forEach(item => {
                    graph2.nodes.push({
                        id: item[0],
                        label: `  ${item[0]}  \n  ${item[1]}  `,
                        group: 1,
                    })
                })
            }
            let arrWorks = []
            let arcs2 = []
            answer.inputMatrix.forEach(item => {
                arcs2.push([item[0], item[1], item[4]])
            })
            answer.inputMatrix.forEach(graphItem => {
                if (!arrWorks.some(item => item[0] === graphItem[0])) {
                    arrWorks.push([graphItem[0], graphItem[2]])
                }
                if (!arrWorks.some(item => item[0] === graphItem[1])) {
                    arrWorks.push([graphItem[1], graphItem[3]])
                }
            })
            graph1 = {nodes: [],edges: []}
            arcs2.forEach((item, index) => {
                const uniqueId2 = Date.now().toString(36) + Math.random().toString(36).substr(2);
                graph1.edges.push({
                    from: item[0],
                    to: item[1],
                    label: item[2].toString(),
                    id: uniqueId2,
                })
            })
            arrWorks.forEach(item => {
                graph1.nodes.push({
                    id: item[0],
                    label: `  ${item[0]}  \n  ${item[1]}  `,
                    group: 1,
                })
            })
        }
        setAnswerData(answer)
        setTaskData(taskData1)
        console.log(taskData1);
    }, [])
    let { tasksData, graphData, setGraphData } = useContext(Context);
    const closeModal = () => {
        setModalActive(false)
    }
    const clickSourceGraph = () => {
        setGraphData([graph1, graph2])
        setModalActive(true)
    }
    return (
        <div className="task-info__content">
            <div className={`task-info__modal ${modalActive ? "active" : ""}`}>
                {
                    graphData[0] &&
                    <Graph />
                }
                <button
                    className="task-info__modal-button"
                    onClick={() => closeModal()}
                >
                    Ок
                </button>
            </div>
            {
                taskData &&
                <>
                    <div className="task-info__top-cnt">
                        {taskData.type === "a" &&
                            <div className="task-info__table task-info__table-lambda task-info__table-cnt-info">
                                <div className="task-info__table-row">
                                    <div className="task-info__table-cell">
                                        <div className="task-info__table-cell-text task-info__table-cell-numb">Набор данных</div>
                                    </div>
                                    <div className="task-info__table-cell">
                                        <div className="task-info__table-cell-text">{taskData.dataName}</div>
                                    </div>
                                </div>
                                <div className="task-info__table-row">
                                    <div className="task-info__table-cell">
                                        <div className="task-info__table-cell-text task-info__table-cell-numb">Продолжительность проекта</div>
                                    </div>
                                    <div className="task-info__table-cell">
                                        <div className="task-info__table-cell-text">{Math.max(...answerData.lambda)}</div>
                                    </div>
                                </div>
                            </div>
                        }
                        {taskData.type === "b" &&
                            <div className="task-info__table task-info__table-lambda task-info__table-cnt-info">
                                <div className="task-info__table-row">
                                    <div className="task-info__table-cell">
                                        <div className="task-info__table-cell-text task-info__table-cell-numb">Набор данных</div>
                                    </div>
                                    <div className="task-info__table-cell">
                                        <div className="task-info__table-cell-text">{taskData.dataName}</div>
                                    </div>
                                </div>
                                {answerData.needOptimization === false &&
                                    <div className="task-info__table-row">
                                        <div className="task-info__table-cell">
                                            <div className="task-info__table-cell-text task-info__table-cell-numb">Продолжительность проекта</div>
                                        </div>
                                        <div className="task-info__table-cell">
                                            <div className="task-info__table-cell-text">{taskData.dur}</div>
                                        </div>
                                    </div>
                                }
                                <div className="task-info__table-row">
                                    <div className="task-info__table-cell">
                                        <div className="task-info__table-cell-text task-info__table-cell-numb">Ограничение продолжительности</div>
                                    </div>
                                    <div className="task-info__table-cell">
                                        <div className="task-info__table-cell-text">{taskData.limit}</div>
                                    </div>
                                </div>
                                <div className="task-info__table-row">
                                    <div className="task-info__table-cell">
                                        <div className="task-info__table-cell-text task-info__table-cell-numb">Дополнительные затраты</div>
                                    </div>
                                    <div className="task-info__table-cell">
                                        <div className="task-info__table-cell-text">{taskData.expenses}</div>
                                    </div>
                                </div>
                            </div>
                        }
                        <button
                            className="task-info__button"
                            onClick={() => clickSourceGraph()}
                        >
                            Просмотр графов
                        </button>
                    </div>

                    <div className="task-info__line"></div>
                    {
                        <div className="task-info__tables">
                            {
                                taskData.type === "a"
                                    ?
                                    <>
                                        <div className="task-info__table-cnt">
                                            <div className="task-info__table-lable"><b>Минимальные сроки окончания работ</b></div>
                                            <div className="task-info__table task-info__table-lambda">
                                                <div className="task-info__table-row task-info__head-row">
                                                    <div className="task-info__table-cell">
                                                        <div className="task-info__table-cell-text">№ работы</div>
                                                    </div>
                                                    <div className="task-info__table-cell">
                                                        <div className="task-info__table-cell-text">Срок окончания</div>
                                                    </div>
                                                </div>
                                                {answerData.lambda.map((item, index) => {
                                                    return (
                                                        <div
                                                            className="task-info__table-row"
                                                            key={index}
                                                        >
                                                            <div className="task-info__table-cell task-info__table-cell-numb">{index + 1}</div>
                                                            <div className="task-info__table-cell">{item}</div>
                                                        </div>
                                                    )
                                                })}
                                            </div>
                                        </div>
                                        <div className="task-info__table-cnt">
                                            <div className="task-info__table-lable">Исходные данные</div>
                                            <div className="task-info__table">
                                                <div className="task-info__table-row task-info__head-row">
                                                    <div className="task-info__table-cell">
                                                        <div className="task-info__table-cell-text">№</div>
                                                    </div>
                                                    <div className="task-info__table-cell">
                                                        <div className="task-info__table-cell-text">i</div>
                                                    </div>
                                                    <div className="task-info__table-cell">
                                                        <div className="task-info__table-cell-text">j</div>
                                                    </div>
                                                    <div className="task-info__table-cell">
                                                        <div className="task-info__table-cell-text">d<sub>i</sub></div>
                                                    </div>
                                                    <div className="task-info__table-cell">
                                                        <div className="task-info__table-cell-text">d<sub>j</sub></div>
                                                    </div>
                                                    <div className="task-info__table-cell">
                                                        <div className="task-info__table-cell-text">
                                                            {taskData.type}<sub>ij</sub>
                                                        </div>
                                                    </div>
                                                </div>
                                                {answerData.inputMatrix.map((row, index) => {
                                                    return (
                                                        <div
                                                            className="task-info__table-row"
                                                            key={index}
                                                        >
                                                            <div className="task-info__table-cell task-info__table-cell-numb">{index + 1}</div>
                                                            <div className="task-info__table-cell">{row[0]}</div>
                                                            <div className="task-info__table-cell">{row[1]}</div>
                                                            <div className="task-info__table-cell">{row[2]}</div>
                                                            <div className="task-info__table-cell">{row[3]}</div>
                                                            <div className="task-info__table-cell">{row[4]}</div>
                                                        </div>
                                                    )
                                                })}
                                            </div>
                                        </div>
                                        <div className="task-info__table-cnt">
                                            <div className="task-info__table-lable">Рассчетные данные</div>
                                            <div className="task-info__table">
                                                <div className="task-info__table-row task-info__head-row">
                                                    <div className="task-info__table-cell">
                                                        <div className="task-info__table-cell-text">№</div>
                                                    </div>
                                                    <div className="task-info__table-cell">
                                                        <div className="task-info__table-cell-text">i</div>
                                                    </div>
                                                    <div className="task-info__table-cell">
                                                        <div className="task-info__table-cell-text">j</div>
                                                    </div>
                                                    <div className="task-info__table-cell">
                                                        <div className="task-info__table-cell-text">d<sub>i</sub></div>
                                                    </div>
                                                    <div className="task-info__table-cell">
                                                        <div className="task-info__table-cell-text">d<sub>j</sub></div>
                                                    </div>
                                                    <div className="task-info__table-cell">
                                                        <div className="task-info__table-cell-text">
                                                            {taskData.type}<sub>ij</sub>
                                                        </div>
                                                    </div>
                                                </div>
                                                {answerData.outputMatrix.map((row, index) => {
                                                    return (
                                                        <div
                                                            className={`task-info__table-row ${answerData.specificRows.includes(index) ? "specific" : ""}`}
                                                            key={index}
                                                        >
                                                            <div className="task-info__table-cell task-info__table-cell-numb">{row[5]}</div>
                                                            <div className="task-info__table-cell">{row[0]}</div>
                                                            <div className="task-info__table-cell">{row[1]}</div>
                                                            <div className="task-info__table-cell">{row[2]}</div>
                                                            <div className="task-info__table-cell">{row[3]}</div>
                                                            <div className="task-info__table-cell">{row[4]}</div>
                                                        </div>
                                                    )
                                                })}
                                            </div>
                                        </div>
                                        <div className="task-info__table-cnt">
                                            <div className="task-info__table-lable">Граф с жесткими зависимостями</div>
                                            <div className="task-info__table">
                                                <div className="task-info__table-row task-info__head-row">
                                                    <div className="task-info__table-cell">
                                                        <div className="task-info__table-cell-text">№</div>
                                                    </div>
                                                    <div className="task-info__table-cell">
                                                        <div className="task-info__table-cell-text">i</div>
                                                    </div>
                                                    <div className="task-info__table-cell">
                                                        <div className="task-info__table-cell-text">j</div>
                                                    </div>
                                                    <div className="task-info__table-cell">
                                                        <div className="task-info__table-cell-text">d<sub>i</sub></div>
                                                    </div>
                                                    <div className="task-info__table-cell">
                                                        <div className="task-info__table-cell-text">d<sub>j</sub></div>
                                                    </div>
                                                    <div className="task-info__table-cell">
                                                        <div className="task-info__table-cell-text">
                                                            {taskData.type}<sub>ij</sub>
                                                        </div>
                                                    </div>
                                                </div>
                                                {answerData.outputMatrix
                                                    .filter((row, index) => answerData.specificRows.includes(index))
                                                    .map((row, index) => {
                                                        return (
                                                            <div
                                                                className={`task-info__table-row`}
                                                                key={index}
                                                            >
                                                                <div className="task-info__table-cell task-info__table-cell-numb">{++index}</div>
                                                                <div className="task-info__table-cell">{row[0]}</div>
                                                                <div className="task-info__table-cell">{row[1]}</div>
                                                                <div className="task-info__table-cell">{row[2]}</div>
                                                                <div className="task-info__table-cell">{row[3]}</div>
                                                                <div className="task-info__table-cell">{row[4]}</div>
                                                            </div>
                                                        )
                                                    })}
                                            </div>
                                        </div>
                                    </>
                                    :
                                    <>
                                        <div className="task-info__table-cnt">
                                            <div className="task-info__table-lable">Исходные данные</div>
                                            <div className="task-info__table">
                                                <div className="task-info__table-row task-info__head-row">
                                                    <div className="task-info__table-cell">
                                                        <div className="task-info__table-cell-text">№</div>
                                                    </div>
                                                    <div className="task-info__table-cell">
                                                        <div className="task-info__table-cell-text">i</div>
                                                    </div>
                                                    <div className="task-info__table-cell">
                                                        <div className="task-info__table-cell-text">j</div>
                                                    </div>
                                                    <div className="task-info__table-cell">
                                                        <div className="task-info__table-cell-text">d<sub>i</sub></div>
                                                    </div>
                                                    <div className="task-info__table-cell">
                                                        <div className="task-info__table-cell-text">d<sub>j</sub></div>
                                                    </div>
                                                    <div className="task-info__table-cell">
                                                        <div className="task-info__table-cell-text">
                                                            {taskData.type}<sub>ij</sub>
                                                        </div>
                                                    </div>
                                                </div>
                                                {answerData.inputMatrix.map((row, index) => {
                                                    return (
                                                        <div
                                                            className="task-info__table-row"
                                                            key={index}
                                                        >
                                                            <div className="task-info__table-cell task-info__table-cell-numb">{index + 1}</div>
                                                            <div className="task-info__table-cell">{row[0]}</div>
                                                            <div className="task-info__table-cell">{row[1]}</div>
                                                            <div className="task-info__table-cell">{row[2]}</div>
                                                            <div className="task-info__table-cell">{row[3]}</div>
                                                            <div className="task-info__table-cell">{row[4]}</div>
                                                        </div>
                                                    )
                                                })}
                                            </div>
                                        </div>
                                        <div className="task-info__table-cnt">
                                            <div className="task-info__table-lable">Рассчетные данные</div>
                                            <div className="task-info__table">
                                                <div className="task-info__table-row task-info__head-row">
                                                    <div className="task-info__table-cell">
                                                        <div className="task-info__table-cell-text">№</div>
                                                    </div>
                                                    <div className="task-info__table-cell">
                                                        <div className="task-info__table-cell-text">i</div>
                                                    </div>
                                                    <div className="task-info__table-cell">
                                                        <div className="task-info__table-cell-text">j</div>
                                                    </div>
                                                    <div className="task-info__table-cell">
                                                        <div className="task-info__table-cell-text">d<sub>i</sub></div>
                                                    </div>
                                                    <div className="task-info__table-cell">
                                                        <div className="task-info__table-cell-text">d<sub>j</sub></div>
                                                    </div>
                                                    <div className="task-info__table-cell">
                                                        <div className="task-info__table-cell-text">
                                                            {taskData.type}<sub>ij</sub>
                                                        </div>
                                                    </div>
                                                </div>
                                                {answerData.outputMatrix.map((row, index) => {
                                                    return (
                                                        <div
                                                            className={`task-info__table-row ${answerData.specificRows.includes(index) ? "specific" : ""}`}
                                                            key={index}
                                                        >
                                                            <div className="task-info__table-cell task-info__table-cell-numb">{index + 1}</div>
                                                            <div className="task-info__table-cell">{row[0]}</div>
                                                            <div className="task-info__table-cell">{row[1]}</div>
                                                            <div className="task-info__table-cell">{row[2]}</div>
                                                            <div className="task-info__table-cell">{row[3]}</div>
                                                            <div className="task-info__table-cell">{row[4]}</div>
                                                        </div>
                                                    )
                                                })}
                                            </div>
                                        </div>
                                        {
                                            answerData.needOptimization &&
                                            <div className="task-info__table-cnt">
                                                <div className="task-info__table-lable">Граф с жесткими зависимостями</div>
                                                <div className="task-info__table">
                                                    <div className="task-info__table-row task-info__head-row">
                                                        <div className="task-info__table-cell">
                                                            <div className="task-info__table-cell-text">№</div>
                                                        </div>
                                                        <div className="task-info__table-cell">
                                                            <div className="task-info__table-cell-text">i</div>
                                                        </div>
                                                        <div className="task-info__table-cell">
                                                            <div className="task-info__table-cell-text">j</div>
                                                        </div>
                                                        <div className="task-info__table-cell">
                                                            <div className="task-info__table-cell-text">d<sub>i</sub></div>
                                                        </div>
                                                        <div className="task-info__table-cell">
                                                            <div className="task-info__table-cell-text">d<sub>j</sub></div>
                                                        </div>
                                                        <div className="task-info__table-cell">
                                                            <div className="task-info__table-cell-text">
                                                                {taskData.type}<sub>ij</sub>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {answerData.outputMatrix
                                                        .filter((row, index) => answerData.specificRows.includes(index))
                                                        .map((row, index) => {
                                                            return (
                                                                <div
                                                                    className={`task-info__table-row`}
                                                                    key={index}
                                                                >
                                                                    <div className="task-info__table-cell task-info__table-cell-numb">{++index}</div>
                                                                    <div className="task-info__table-cell">{row[0]}</div>
                                                                    <div className="task-info__table-cell">{row[1]}</div>
                                                                    <div className="task-info__table-cell">{row[2]}</div>
                                                                    <div className="task-info__table-cell">{row[3]}</div>
                                                                    <div className="task-info__table-cell">{row[4]}</div>
                                                                </div>
                                                            )
                                                        })}
                                                </div>
                                            </div>
                                        }
                                    </>
                            }

                        </div>
                    }
                </>
            }
        </div >
    )
}
export default TaskInfo;