import "./tasksTable.scss";
import { useContext } from "react";
import { Context } from "../../context";
import { useNavigate } from "react-router-dom";
const TasksTable = () => {
    const navigate = useNavigate();
    let { tasksData } = useContext(Context);
    const clickRow = (number) => {
        navigate(`/info/${number}`)
    }
    return (
        <div className="tasks-page__table">
            <div className="tasks-page__row tasks-page__head-row">
                <div className="tasks-page__cell">Номер</div>
                <div className="tasks-page__cell">Набор данных</div>
                <div className="tasks-page__cell">Статус</div>
                <div className="tasks-page__cell">Тип задачи</div>
                <div className="tasks-page__cell">Продолжительность</div>
                <div className="tasks-page__cell">Ограничение продолжительности</div>
                <div className="tasks-page__cell">Дополнительные затраты</div>
            </div>
            {tasksData.map((row, index) => {
                return (
                    <div
                        className="tasks-page__row"
                        onClick={() => clickRow(row.number)}
                        key={index}
                    >
                        <div className="tasks-page__cell">{row.number}</div>
                        <div className="tasks-page__cell">{row.dataName}</div>
                        <div className={`tasks-page__cell ${row.status ? "green" : ""}`}>{row.status ? "Просмотрено" : "Не просмотрено"}</div>
                        <div className="tasks-page__cell">{row.type}</div>
                        <div className="tasks-page__cell">{row.dur}</div>
                        <div className="tasks-page__cell">{row.limit}</div>
                        <div className="tasks-page__cell">{row.expenses}</div>
                    </div>
                )
            })}
        </div>
    )
}
export default TasksTable;