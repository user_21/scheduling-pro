import { useState } from "react";
import { NavLink } from "react-router-dom";
import "./menu.scss";
const Menu = () => {
    const [menuActive, setMenuActive] = useState(true);
    const toggleMenu = () => {
        setMenuActive(!menuActive)
    }
    return (
        <div className={`menu ${menuActive ? "active" : ""}`}>
            <div className="menu__logo">
                Scheduling PRO
            </div>
            <button
                className="menu__toggle-btn"
                onClick={() => toggleMenu()}
            >
            </button>
            <div className="menu__items-cnt">
                <NavLink
                    className="menu__item menu__add-task"
                    to={"/add-task"}
                    activeclassname="active"
                >
                    <div className="menu__item-icon"></div>
                    <div className="menu__item-text">Добавить задачу</div>
                </NavLink>
                <NavLink
                    className="menu__item menu__list"
                    to={"/"}
                    activeclassname="active"
                >
                    <div className="menu__item-icon"></div>
                    <div className="menu__item-text">Все задачи</div>
                </NavLink>
                <NavLink
                    className="menu__item menu__data"
                    to={"/data"}
                    activeclassname="active"
                >
                    <div className="menu__item-icon"></div>
                    <div className="menu__item-text">Данные</div>
                </NavLink>
            </div>
        </div>
    )
}
export default Menu;