import { useContext, useState } from "react";
import React from 'react';
import Graph from 'react-graph-vis';
import "./graph.scss"
import { Context } from "../../context";
const options = {
    layout: {
        hierarchical: false
    },
    physics: {
        enabled: false
    },
    manipulation: {
        enabled: true,
        initiallyActive: false,
    },
};
const MyGraph = React.memo(() => {
    let { graphData } = useContext(Context);
    return (
        <>
            {graphData && (
                <>
                    <div className="modal__graphs">
                        <div className="modal__graph">
                            <div className="modal__graph-text">Исходный граф.</div>
                            <Graph
                                graph={graphData[0]}
                                options={options}
                                style={{ width: '100%', height: '100%' }}
                            />
                        </div>
                        <div className="modal__graph">
                            <div className="modal__graph-text">Граф с жесткими зависимостями.</div>
                            <Graph
                                graph={graphData[1]}
                                options={options}
                                style={{ width: '100%', height: '100%' }}
                            />
                        </div>
                    </div>
                </>
            )}
        </>
    );
});
export default MyGraph;