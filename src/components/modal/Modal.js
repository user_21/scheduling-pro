import "./modal.scss";
import { useContext } from "react";
import { Context } from "../../context";
const Modal = () => {
    let { modalActive, toggleModal, children } = useContext(Context);
    return (
        <div
            className={`modal ${modalActive ? "active" : ""}`}
            onClick={(e) => {
                e.stopPropagation();
                toggleModal()
            }}
        >
            <div
                className="modal__content"
                onClick={(e) => {
                    e.stopPropagation();
                }}
            >
                {children}
            </div>
        </div>
    )
}
export default Modal;