import "./tasksPage.scss";
import { Outlet, useNavigate, useLocation } from "react-router-dom"
const TasksPage = () => {
    const navigate = useNavigate();
    const location = useLocation();
    const currentPath = location.pathname;
    return (
        <div className="tasks-page">
            <div className="tasks-page__header">
                <div
                    className="tasks-page__header-title"
                    onClick={() => {
                        navigate("/")
                    }}
                >
                    <div className="tasks-page__header-icon"></div>
                    <div className="tasks-page__header-text">Все задачи</div>
                    <div>{currentPath.includes("/info/") ? `/ ${currentPath.replace("/info/", "")}` : ""}</div>   
                </div>
            </div>
            <div className="tasks-page__content">
                <Outlet />
            </div>
        </div>
    )
}
export default TasksPage;