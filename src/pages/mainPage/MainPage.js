import "./mainPage.scss";
import Menu from "../../components/menu/Menu";
import { Outlet } from "react-router-dom"
import Modal from "../../components/modal/Modal";
import { Context } from "../../context";
import { createContext, useState } from "react";
const MyContext = createContext();
export const Provider = MyContext.Provider;
const MainPage = () => {
    const [data, setData] = useState([]);
    const [allData, setAllData] = useState([]);
    const [children, setChildren] = useState(null);
    const [modalActive, setModalActive] = useState(false);
    const [tasksData, setTasksData] = useState([]);
    const [graphData, setGraphData] = useState([]);
    const toggleData = (data) => {
        setData(data)
    }
    const toggleModal = () => {
        setModalActive(!modalActive)
    }
    const toggleAllData = (data) => {
        setAllData(data)
    }
    const toggleChildren = (children) => {
        setChildren(children)
    }
    return (
        <Context.Provider value={{ data, toggleData, modalActive, toggleModal, allData, toggleAllData, children, toggleChildren, tasksData, setTasksData, graphData, setGraphData }}>
            <div className="main-page">
                <Menu />
                <div className="main-page__content">
                    <Outlet />
                </div>
                <Modal />
            </div>
        </Context.Provider>
    )
}
export default MainPage;