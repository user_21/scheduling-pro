import "./addTaskPage.scss";
import { useContext, useState } from "react";
import { Context } from "../../context";
import { useNavigate } from "react-router-dom";
const AddTaskPage = () => {
    let {  allData, tasksData, setTasksData } = useContext(Context);
    const [selectedOption, setSelectedOption] = useState("a");
    const [activeRow, setActiveRow] = useState("");
    const [clickedAddButton, setClickedAddButton] = useState(false);
    const [modalActive, setModalActive] = useState(false);
    const [limit, setLimit] = useState("");
    const navigate = useNavigate();
    const handleOptionChange = (e) => {
        setSelectedOption(e.target.value);
        setActiveRow("");
        setClickedAddButton(false)
        const dataA = allData.filter(item => item.type === "a")
        const dataB = allData.filter(item => item.type === "b")
        if (dataA.length !== 0 && dataB.length !== 0 && (dataA.length == 1 || dataB.length == 1)) {
            if (e.target.value === "a") {
                setActiveRow(dataA[0].id)
            }
            if (e.target.value === "b") {
                setActiveRow(dataB[0].id)
            }
        }
    };
    const rowClick = (id) => {
        setActiveRow(id);
    }
    const addATask = () => {
        setClickedAddButton(true)
        setTimeout(() => {
            navigate("/")
            activeRowData.status = true;
        }, 300)
        const activeRowData = allData.find(item => item.id === activeRow);
        let now = new Date();
        let day = now.getDate().toString().padStart(2, '0');
        let month = (now.getMonth() + 1).toString().padStart(2, '0');
        let hour = now.getHours().toString().padStart(2, '0');
        let minute = now.getMinutes().toString().padStart(2, '0');
        let second = now.getSeconds().toString().padStart(2, '0');
        let formattedString = `${day}${month}${hour}${minute}${second}`;
        setTasksData([...tasksData, { number: formattedString, dataName: activeRowData.name, status: false, type: activeRowData.type, limit: "-", dur: "-", expenses: "-", data: activeRowData.allData }]);
    }
    const clickAddTask = async () => {
        if (selectedOption === "b") {
            setModalActive(true)
        } else {
            addATask()
        }
    }
    const clickModalReset = () => {
        setModalActive(false)
    }
    const clickModalOk = () => {
        setModalActive(false)
        setClickedAddButton(true)
        setTimeout(() => {
            navigate("/")
            activeRowData.status = true;
        }, 300)
        const activeRowData = allData.find(item => item.id === activeRow);
        let now = new Date();
        let day = now.getDate().toString().padStart(2, '0');
        let month = (now.getMonth() + 1).toString().padStart(2, '0');
        let hour = now.getHours().toString().padStart(2, '0');
        let minute = now.getMinutes().toString().padStart(2, '0');
        let second = now.getSeconds().toString().padStart(2, '0');
        let formattedString = `${day}${month}${hour}${minute}${second}`;
        setTasksData([...tasksData, { number: formattedString, dataName: activeRowData.name, status: false, type: activeRowData.type, limit: limit, dur: "-", expenses: "-", data: activeRowData.allData }]);
    }
    return (
        <div className="add-task-page">
            <div className={`add-task-page__modal ${modalActive ? "active" : ""}`}>
                <div className="add-task-page__modal-content">
                    Введите ограничение продолжительности.
                    <input
                        className="modal-input"
                        type="text"
                        onChange={(e) => setLimit(e.target.value)}
                        value={limit}
                    />
                    <div className="modal__buttons-block">
                        <button
                            className="modal__button modal__button-no"
                            onClick={() => clickModalReset()}
                        >
                            Отмена
                        </button>
                        <button
                            className="modal__button modal__button-yes"
                            onClick={() => clickModalOk()}
                        >
                            Ок
                        </button>
                    </div>
                </div>
            </div>
            <div className="add-task-page__header">
                <div className="add-task-page__header-title">
                    <div className="add-task-page__header-icon"></div>
                    <div className="add-task-page__header-text">Добавление задачи</div>
                </div>
            </div>
            <div className="add-task-page__content">
                <div className="add-task-page__label">
                    <div className="add-task-page__label-icon"></div>
                    Выберите тип задачи и набор данных.
                </div>
                {allData.length === 0 &&
                    <button className="add-task-page__add-data-btn">
                        <a href="/data">Добавить данные</a>
                    </button>
                }
                <div className={`add-task-page__table-wrapper add-task-page__table-a-wrapper ${selectedOption === "a" ? "active" : ""}`}>
                    <label>
                        <input
                            type="radio"
                            value="a"
                            checked={selectedOption === "a"}
                            onChange={handleOptionChange}
                        />
                        Минимальная продолжительность проекта
                        <div className="add-task-page__table add-task-page__table-a">
                            <div className="add-task-page__row add-task-page__head-row">
                                <div className="add-task-page__cell">Название</div>
                                <div className="add-task-page__cell">Дата загрузки</div>
                                <div className="add-task-page__cell">Статус</div>
                            </div>
                            {allData
                                .filter(row => row.type === "a")
                                .map((row, index) => {
                                    return (
                                        <div
                                            className={`add-task-page__row ${activeRow === row.id ? "row-active" : ""}`}
                                            onClick={() => rowClick(row.id)}
                                            key={index}
                                        >
                                            <div className="add-task-page__cell">{row.name}</div>
                                            <div className="add-task-page__cell">{row.date}</div>
                                            <div className={`add-task-page__cell ${row.status ? "green" : ""}`}>{row.status ? "Используется" : "Не используется"}</div>
                                        </div>
                                    )
                                })}
                        </div>
                    </label>
                </div>
                <div className={`add-task-page__table-wrapper add-task-page__table-b-wrapper ${selectedOption === "b" ? "active" : ""}`}>
                    <label>
                        <input
                            type="radio"
                            value="b"
                            checked={selectedOption === "b"}
                            onChange={handleOptionChange}
                        />
                        Минимальные дополнительные затраты
                        <div className="add-task-page__table add-task-page__table-b">
                            <div className="add-task-page__row add-task-page__head-row">
                                <div className="add-task-page__cell">Название</div>
                                <div className="add-task-page__cell">Дата загрузки</div>
                                <div className="add-task-page__cell">Статус</div>
                            </div>
                            {allData
                                .filter(row => row.type === "b")
                                .map(row => {
                                    return (
                                        <div
                                            className={`add-task-page__row ${activeRow === row.id ? "row-active" : ""}`}
                                            onClick={() => rowClick(row.id)}
                                        >
                                            <div className="add-task-page__cell">{row.name}</div>
                                            <div className="add-task-page__cell">{row.date}</div>
                                            <div className={`add-task-page__cell ${row.status ? "green" : ""}`}>{row.status ? "Используется" : "Не используется"}</div>
                                        </div>
                                    )
                                })}
                        </div>
                    </label>
                </div>
                <button
                    className={`add-task-page__add-btn ${activeRow ? "active" : ""} ${clickedAddButton ? "active-btn" : ""}`}
                    disabled={!activeRow}
                    onClick={() => clickAddTask()}
                >
                    Создать задачу
                    <div className="add-task-page__add-btn-arrow"></div>
                </button>
            </div>
        </div>
    )
}
export default AddTaskPage;