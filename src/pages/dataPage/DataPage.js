import "./dataPage.scss";
import * as XLSX from 'xlsx';
import { useContext } from "react";
import { Context } from "../../context";
import DynamicTable from "../../components/dynamicTable/DynamicTable";
const DataPage = () => {
    let { toggleData, toggleModal, allData, toggleAllData, toggleChildren } = useContext(Context);
    const handleFileUpload = (e) => {
        try {
            const file = e.target.files[0];
            const reader = new FileReader();
            const fileName = file.name;
            const uploadDate = new Date();
            const formattedDate = new Intl.DateTimeFormat('ru-RU', {
                day: 'numeric',
                month: 'numeric',
                year: 'numeric',
                hour: 'numeric',
                minute: 'numeric'
            }).format(uploadDate);
            reader.onload = (e) => {
                const data = new Uint8Array(e.target.result);
                const workbook = XLSX.read(data, { type: 'array' });
                const sheetName = workbook.SheetNames[0];
                const sheet = workbook.Sheets[sheetName];
                const jsonData = XLSX.utils.sheet_to_json(sheet); // сама таблица с данными
                jsonData.map((row, index) => {
                    row.id = index + 1;
                })
                const uniqueId = Date.now().toString(36) + Math.random().toString(36).substr(2);
                let type = jsonData[0].aij ? "a" : "b"
                toggleAllData([...allData, { id: uniqueId, name: fileName, date: formattedDate, status: false, type: type, allData: jsonData }])
            };
            reader.readAsArrayBuffer(file);
        } catch (e) {
            console.log(e);
        }
    };
    const editTable = (data) => {
        toggleData(data);
        toggleChildren(<DynamicTable mod={"edit"} />)
        toggleModal()
    }
    const deleteTable = (id) => {
        const updatedTableData = allData.filter(row => row.id !== id);
        toggleAllData(updatedTableData);
    }
    const addTable = () => {
        toggleChildren(<DynamicTable mod={"new"} />)
        toggleData([]);
        toggleModal()
    }
    return (
        <div className="data-page">
            <div className="data-page__header">
                <div className="data-page__header-title">
                    <div className="data-page__header-icon"></div>
                    <div className="data-page__header-text">Данные</div>
                </div>
            </div>
            <div className="data-page__content">
                <div className="data-page__buttons-cnt">
                    <label
                        htmlFor="fileInput"
                        className="data-page__import-btn"
                    >
                        <div className="import-btn__icon"></div>
                        <div className="import-btn__text">Импорт таблицы</div>
                    </label>
                    <input
                        id="fileInput"
                        type="file"
                        onChange={handleFileUpload}
                        style={{ display: 'none' }}
                    />
                    <div
                        className="data-page__add-btn data-page__import-btn"
                        onClick={() => addTable()}
                    >
                        <div className="add-btn__icon import-btn__icon"></div>
                        <div
                            className="add-btn__text"
                        >
                            Создание таблицы
                        </div>
                    </div>
                </div>
                <div className="data-page__table">
                    <div className="data-page__row data-page__head-row">
                        <div className="data-page__cell">Название</div>
                        <div className="data-page__cell">Дата загрузки</div>
                        <div className="data-page__cell">Статус</div>
                        <div className="data-page__cell">Тип задачи</div>
                        <div className="data-page__cell">Редактирование</div>
                        <div className="data-page__cell">Удаление</div>
                    </div>
                    {allData.map((row, index) => {
                        return (
                            <div
                                className="data-page__row"
                                key={index}
                            >
                                <div className="data-page__cell">{row.name}</div>
                                <div className="data-page__cell">{row.date}</div>
                                <div className={`data-page__cell ${row.status ? "green" : ""}`}>{row.status ? "Используется" : "Не используется"}</div>
                                <div className="data-page__cell">{row.type}</div>
                                <div
                                    className="data-page__cell data-page__cell-edit"
                                    onClick={() => editTable(row)}
                                >
                                    <div className="data-page__cell-edit-icon"></div>
                                </div>
                                <div
                                    className="data-page__cell data-page__cell-edit data-page__cell-trash"
                                    onClick={() => deleteTable(row.id)}
                                >
                                    <div className="data-page__cell-edit-icon"></div>
                                </div>
                            </div>
                        )
                    })}
                </div>
            </div>
        </div>
    )
}
export default DataPage;