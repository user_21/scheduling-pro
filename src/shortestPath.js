class Node {
    constructor(data, indexloc) {
        this.data = data;
        this.index = indexloc;
    }
}

class Graph {
    constructor(row, col, nodes) {
        this.adj_mat = Array.from({ length: row }, () => Array(col).fill(0));
        this.nodes = nodes;
        for (let i = 0; i < this.nodes.length; i++) {
            this.nodes[i].index = i;
        }
    }

    static createFromNodes(nodes) {
        return new Graph(nodes.length, nodes.length, nodes);
    }

    connectDir(node1, node2, weight = 1) {
        node1 = this.getIndexFromNode(node1);
        node2 = this.getIndexFromNode(node2);
        this.adj_mat[node1][node2] = weight;
    }

    connect(node1, node2, weight = 1) {
        this.connectDir(node1, node2, weight);
        this.connectDir(node2, node1, weight);
    }

    connectionsFrom(node) {
        node = this.getIndexFromNode(node);
        return this.adj_mat[node].map((weight, colNum) => {
            if (weight !== 0) {
                return [this.nodes[colNum], weight];
            }
        }).filter(Boolean);
    }

    connectionsTo(node) {
        node = this.getIndexFromNode(node);
        const column = this.adj_mat.map(row => row[node]);
        return column.map((weight, rowNum) => {
            if (weight !== 0) {
                return [this.nodes[rowNum], weight];
            }
        }).filter(Boolean);
    }

    printAdjMat() {
        this.adj_mat.forEach(row => console.log(row));
    }

    getNode(index) {
        return this.nodes[index];
    }

    removeConn(node1, node2) {
        this.removeConnDir(node1, node2);
        this.removeConnDir(node2, node1);
    }

    removeConnDir(node1, node2) {
        node1 = this.getIndexFromNode(node1);
        node2 = this.getIndexFromNode(node2);
        this.adj_mat[node1][node2] = 0;
    }

    canTraverseDir(node1, node2) {
        node1 = this.getIndexFromNode(node1);
        node2 = this.getIndexFromNode(node2);
        return this.adj_mat[node1][node2] !== 0;
    }

    hasConn(node1, node2) {
        return this.canTraverseDir(node1, node2) || this.canTraverseDir(node2, node1);
    }

    addNode(node) {
        this.nodes.push(node);
        node.index = this.nodes.length - 1;
        this.adj_mat.forEach(row => row.push(0));
        this.adj_mat.push(Array(this.adj_mat.length + 1).fill(0));
    }

    getWeight(n1, n2) {
        const node1 = this.getIndexFromNode(n1);
        const node2 = this.getIndexFromNode(n2);
        return this.adj_mat[node1][node2];
    }

    getIndexFromNode(node) {
        if (!(node instanceof Node) && typeof node !== 'number') {
            throw new Error("node must be an integer or a Node object");
        }
        if (typeof node === 'number') {
            return node;
        } else {
            return node.index;
        }
    }

    dijkstra(node) {
        const nodeNum = this.getIndexFromNode(node);
        const dist = Array(this.nodes.length).fill().map(() => [Infinity, [this.nodes[nodeNum]]]);

        dist[nodeNum][0] = 0;
        const queue = [...Array(this.nodes.length).keys()];
        const seen = new Set();

        while (queue.length > 0) {
            let minDist = Infinity;
            let minNode = null;
            for (const n of queue) {
                if (dist[n][0] < minDist && !seen.has(n)) {
                    minDist = dist[n][0];
                    minNode = n;
                }
            }

            queue.splice(queue.indexOf(minNode), 1);
            seen.add(minNode);

            const connections = this.connectionsFrom(minNode);
            for (const [node, weight] of connections) {
                const totDist = weight + minDist;
                if (totDist < dist[node.index][0]) {
                    dist[node.index][0] = totDist;
                    dist[node.index][1] = [...dist[minNode][1], node];
                }
            }
        }

        return dist;
    }
}

function getShortestWay(arr) {
    const array = arr.map(x => x.slice(0, -1));
    const arrPeaks = [...new Set(array.flat())];
    const peaks = arrPeaks.map(data => new Node(String(data)));

    const wGraph = Graph.createFromNodes(peaks);

    for (const [node1, node2, weight] of arr) {
        wGraph.connect(
            peaks[node1 - 1],
            peaks[node2 - 1],
            weight === 0 ? 1 : weight
        );
    }

    return wGraph.dijkstra(peaks[0]).map(([weight, nodes]) => [weight, nodes.map(n => n.data)]);
}

const ShortestWay = {
    getShortestWay
}

export default ShortestWay;