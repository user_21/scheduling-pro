import ShortestWay from './shortestPath.js';

function chainSoul(dataFrame) {
    // return 
    // let dataValues = dataFrame.slice(1, 20);
    let matrix = dataFrame.map(obj => Object.values(obj)).slice(1, 20);

    let dataValues = [...matrix]
    const T = 14;

    const n = dataValues.length;
    let sumDurations = 0;

    let edges = dataValues.map(x => x.slice(0, -3));

    for (let i = 0; i < n; i++) {
        for (let j = 0; j < n + 1; j++) {
            if (![i + 1, j + 1].some(el => edges.some(edge => JSON.stringify(edge) === JSON.stringify([i + 1, j + 1]))) && j > i) {
                sumDurations = 0;
                for (let i1 = i; i1 < j; i1++) {
                    sumDurations += dataValues[i1][2];
                }
                if (sumDurations <= T) {
                    const iDuration = dataValues[i][2];
                    const jDuration = dataValues[j - 1][3];
                    const edgeValue = dataValues[j - 1][4];
                    dataValues.push([i + 1, j + 1, iDuration, jDuration, edgeValue]);
                }
            }
        }
    }

    const dataAnswer = dataValues;

    edges = dataValues.map(x => x.slice(0, -3).concat(x[4]));
    const shortestWay = ShortestWay.getShortestWay(edges);
    let answerWayDuration = shortestWay[shortestWay.length - 1][0];
    const shortestWayNodes = shortestWay[shortestWay.length - 1][1];
    const edgesAnswer = [];

    for (let i1 = 0; i1 < shortestWayNodes.length - 1; i1++) {
        edgesAnswer.push([parseInt(shortestWayNodes[i1]), parseInt(shortestWayNodes[i1 + 1])]);
    }

    const edgesWithNull = edges.filter(i => i[2] === 0);

    let count = 0;
    for (const i of edgesAnswer) {
        if (edgesWithNull.some(x => x[0] === i[0] && x[1] === i[1])) {
            count++;
        }
    }

    answerWayDuration -= count;

    const answerWay = [answerWayDuration, edgesAnswer];

    const specificRow = dataAnswer.reduce((acc, x, idx) => {
        if (edgesAnswer.some(edge => JSON.stringify(edge) === JSON.stringify(x.slice(0, -3)))) {
            acc.push(idx);
        }
        return acc;
    }, []);

    // return [dataAnswer, answerWay, specificRow];

    let obj = {
        inputMatrix: matrix,
        outputMatrix: dataAnswer,
        specificRows: specificRow,
        answerWay: answerWay
    }

    return obj;
}

const ChainSolution = {
    chainSoul
}

export default ChainSolution