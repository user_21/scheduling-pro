import './App.scss';
import MainPage from './pages/mainPage/MainPage';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import TasksPage from './pages/tasksPage/TasksPage';
import DataPage from './pages/dataPage/DataPage';
import AddTaskPage from './pages/addTaskPage/AddTaskPage';
import TasksTable from './components/tasksTable/TasksTable';
import TaskInfo from './components/taskInfo/TaskInfo';

function App() {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<MainPage />}>
                    <Route element={<TasksPage />} >
                        <Route index element={<TasksTable />} />
                        <Route path="/info/:number" element={<TaskInfo />} />
                    </Route>
                    <Route path="/data" element={<DataPage />} />
                    <Route path="/add-task" element={<AddTaskPage />} />
                </Route>
            </Routes>
        </BrowserRouter>
    )
}

export default App;
